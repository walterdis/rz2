<?php
require_once(__DIR__ . '/bootstrap.php');

use WalterDis\rz2\Entity\Customer;
use WalterDis\rz2\Entity\Sale;
use WalterDis\rz2\Entity\Salesman;
use WalterDis\rz2\Factory;


$file = read_path('20180528.dat');

$reader = new \ParseCsv\Csv();
$reader->delimiter = ',';
$reader->heading = false;

$reader->parse($file);
$report = new \WalterDis\rz2\Report();

echo 'Reading file: ' . $file."<br />";

foreach ($reader->data as $data) {
    $id = current($data);
    switch ($id) {
        case '001':
            $salesman = Salesman::load($data);
            $sellers[] = $salesman;
            $report->collect($salesman);
            break;
        case Customer::ID:
            $customer = Customer::load($data);
            $customers[] = $customer;
            $report->collect($customer);
            break;
        case Sale::ID:
            $sale = Sale::load($data);
            $sales[] = $sale;
            $report->collect($sale);
            break;
    }
}

$writer = new \ParseCsv\Csv();
$writer->save(storage_path('out.dat'), [$report->get()]);
echo "Data exported to file: " . storage_path('/out.dat');