<?php
/**
 * Created by PhpStorm.
 * User: mistr
 * Date: 28/05/2018
 * Time: 21:05
 */

namespace WalterDis\rz2\Entity;


class Salesman
{

    const ID = '001';

    public $cpf;
    public $name;
    public $salary;


    /**
     * @param array $data
     * @return Salesman
     */
    public static function load(array $data)
    {
        if (!static::validate($data)) {
            throw new \InvalidArgumentException('Could not parse the given Salesman.');
        }

        $entity = new self();

        $entity->cpf = $data[1];
        $entity->name = $data[2];
        $entity->salary = $data[3];

        return $entity;
    }

    /**
     * @param array $data
     * @return bool
     */
    public static function validate(array $data): bool
    {
        if (count($data) < 4) {
            return false;
        }

        if (current($data) != self::ID) {
            return false;
        }

        return true;
    }

}