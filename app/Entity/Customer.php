<?php
/**
 * Created by PhpStorm.
 * User: mistr
 * Date: 28/05/2018
 * Time: 21:05
 */

namespace WalterDis\rz2\Entity;

/**
 * Class Customer
 * @package WalterDis\rz2\Entity
 * @property string $cnpj
 * @property string $name
 * @property string $businessArea
 */
class Customer
{

    const ID = '002';

    public $cnpj;
    public $name;
    public $businessArea;

    /**
     * @param array $data
     * @return Customer
     */
    public static function load(array $data)
    {
        if (!static::validate($data)) {
            throw new \InvalidArgumentException('Could not parse the given Customer.');
        }

        $entity = new self();

        $entity->cnpj = $data[1];
        $entity->name = $data[2];
        $entity->businessArea = $data[3];

        return $entity;
    }

    /**
     * @param array $data
     * @return bool
     */
    public static function validate(array $data): bool
    {
        if (count($data) < 4) {
            return false;
        }

        if (current($data) != self::ID) {
            return false;
        }

        return true;
    }

    /**
     * @return mixed
     */
    public function getBusinessArea()
    {
        return $this->businessArea;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * @return mixed
     */
    public function getCnpj()
    {
        return $this->cnpj;
    }


}