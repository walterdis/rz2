<?php
/**
 * Created by PhpStorm.
 * User: mistr
 * Date: 28/05/2018
 * Time: 21:05
 */

namespace WalterDis\rz2\Entity;


class Sale
{
    const ID = '003';

    public $saleId;
    public $itemId;
    public $itemName;
    public $quantity;
    public $sellPrice;
    public $total = 0;
    public $salesmanName;

    /**
     * @param array $data
     * @return Sale
     */
    public static function load(array $data)
    {
        if (!static::validate($data)) {
            throw new \InvalidArgumentException('Could not parse the given Sale.');
        }

        $entity = new self();

        $entity->saleId = $data[1];
        $entity->salesmanName = $data[3];

        self::loadItemData($entity, $data);

        return $entity;
    }

    /**
     * @param Sale $entity
     * @param array $data
     * @return Sale
     */
    private static function loadItemData(Sale $entity, array $data): Sale
    {
        if (!isset($data[2])) {
            throw new \InvalidArgumentException('Could not parse the given Sale item data.');
        }

        $itemData = $data[2];

        if ($itemData[0] != '[' || $itemData[-1] != ']') {
            throw new \InvalidArgumentException('Could not parse the given Sale item data.');
        }

        $itemData = trim($data[2], '[]');
        $itemData = explode(',', $itemData);
        if (count($itemData) < 4) {
            throw new \InvalidArgumentException('Could not parse the given Sale item data.');
        }

        $entity->itemName = $itemData[0];
        $entity->itemId = $itemData[1];
        $entity->quantity = $itemData[2];
        $entity->sellPrice = $itemData[3];

        $entity->total = static::calculateTotal($entity);

        return $entity;
    }

    /**
     * @param Sale $sale
     * @return float|int
     */
    private static function calculateTotal(Sale $sale)
    {
        return $sale->sellPrice * $sale->quantity;
    }

    /**
     * @param array $data
     * @return bool
     */
    public static function validate(array $data): bool
    {
        if (count($data) < 4) {
            return false;
        }

        if (current($data) != self::ID) {
            return false;
        }

        return true;
    }
}