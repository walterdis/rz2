<?php

namespace WalterDis\rz2;


use WalterDis\rz2\Entity\Customer;
use WalterDis\rz2\Entity\Sale;
use WalterDis\rz2\Entity\Salesman;

class Report
{
    /**
     * @var Sale
     */
    public $biggestSale = null;

    /**
     * @var Sale
     */
    public $worstSale = null;

    /**
     * @var int
     */
    public $customersAmount = 0;

    /**
     * @var int
     */
    public $salesManAmount = 0;

    /**
     * @var float
     */
    public $salesmanSalariesTotal = 0;

    /**
     * @var Salesman
     */
    public $worstSalesMan;

    /**
     * @var
     */
    private $object;

    /**
     * @param Sale|Salesman|Customer $object
     */
    public function collect($object)
    {
        $this->object = $object;

        if ($this->isCustomer()) {
            $this->customersAmount++;
        }

        if ($this->isSalesMan()) {
            /* @@var Salesman $object */
            $this->salesmanSalariesTotal += $object->salary;
            $this->salesManAmount++;
        }

        if ($this->isSale()) {
            $this->parseSale($object);
        }
    }

    /**
     * @param Sale $sale
     */
    private function parseSale(Sale $sale)
    {
        if (!$this->biggestSale || $this->biggestSale->total < $sale->total) {
            $this->biggestSale = $sale;
        }

        if (!$this->worstSale || $this->worstSale->total > $sale->total) {
            $this->worstSale = $sale;
        }
    }

    /**
     * @return array
     */
    public function get(): array
    {
        return [
            'customersAmount' => $this->customersAmount,
            'salesmanAmount' => $this->salesManAmount,
            'averageSalary' => round($this->salesmanSalariesTotal / $this->salesManAmount, 2),
            'biggestSaleId' => $this->biggestSale->saleId,
            'worstSalesman' => $this->worstSale->salesmanName,
        ];
    }

    /**
     * @return bool
     */
    public function isSalesMan()
    {
        return $this->object instanceof Salesman;
    }

    /**
     * @return bool
     */
    public function isSale()
    {
        return $this->object instanceof Sale;
    }

    /**
     * @return bool
     */
    public function isCustomer()
    {
        return $this->object instanceof Customer;
    }

}