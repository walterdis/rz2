<?php

$autoloadFile = __DIR__ . '/vendor/autoload.php';

if(!file_exists($autoloadFile)) {
    die('Run composer install');
}

require_once($autoloadFile);

define('DS', DIRECTORY_SEPARATOR);

/**
 * @author Walter Discher Cechinel
 * @param string $param
 * @return string
 */
function data_path(string $param): string
{
    return __DIR__ . DS . 'data' . $param;
}

/**
 * @author Walter Discher Cechinel
 * @param string $param
 * @return string
 */
function storage_path(string $param): string
{
    return data_path(DS . 'out' . DS . $param);
}

/**
 * @author Walter Discher Cechinel
 * @param string $param
 * @return string
 */
function read_path(string $param): string
{
    return data_path(DS . 'in' . DS . $param);
}

/**
 * @param array $param
 * @return string
 */
function pp($param): void
{
    echo '<pre>';
    print_r($param);
    echo '</pre>';

}